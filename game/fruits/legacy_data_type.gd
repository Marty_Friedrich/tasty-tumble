extends Resource
class_name LegacyData

@export var colors:Array[Color]
@export var radius:Array[float]
@export var points:Array[int]

@export var chances:Array[int]
@export var amount:int

func pick_fruit():
	var array:Array[int] = []
	for i in chances.size():
		for n in chances[i]:
			array.append(i)
	return array.pick_random()
