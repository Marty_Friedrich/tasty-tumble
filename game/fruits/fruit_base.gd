extends RigidBody2D
class_name Fruit

@export var fruitData:FruitData

@export var glassSoundVelocity:float = 40

@export var physicsMaterials:Array[PhysicsMaterial]

var isPopping:bool = false

func _ready():
	if physicsMaterials[G.temp["FRUIT_MODE"]]:
		physics_material_override = physicsMaterials[G.temp["FRUIT_MODE"]]
	else:
		physics_material_override = physicsMaterials[0]

func body_entered(body):
	if body.is_in_group(get_groups()[0]) and get_parent() == body.get_parent() and body.freeze == false and body.isPopping == false:
		isPopping = true
		body.isPopping = true
		var selfPos:Vector2 = global_position
		var otherPos:Vector2 = body.global_position
		var newPos:Vector2 = (selfPos + otherPos) / 2
		var otherRot:float = body.global_rotation
		var selfRot:float = global_rotation
		var selfVel:Vector2 = linear_velocity
		var otherVel:Vector2 = body.linear_velocity
		var parent:Node2D = get_parent()
		call_deferred("pop")
		body.call_deferred("pop")
		var duplicat:GPUParticles2D = get_parent().get_parent().get_parent().get_node("GameManager").boom.duplicate()
		duplicat.global_position = newPos
		get_parent().get_parent().get_node("Pop").play_random()
		get_parent().get_parent().get_node("Booms").add_child(duplicat)
		get_tree().current_scene.get_node("GameManager").add_score(fruitData.pointValue * 2)
		if fruitData.nextFruit:
			var newFruit: RigidBody2D = fruitData.nextFruit.instantiate()
			newFruit.global_position = newPos
			newFruit.global_rotation = lerp_angle(otherRot, selfRot, 0.5)
			newFruit.linear_velocity = lerp(selfVel, otherVel, 0.5)
			parent.call_deferred("add_child", newFruit)
			newFruit.get_node("NameBoom").emitting = true

func pop():
	freeze = true
	for i in get_children():
		if i is CollisionShape2D:
			i.disabled = true
	ScrEffect.line_up()
	var TW:Tween = create_tween()
	TW.tween_property(self, "scale", Vector2(1.7, 1.7), 0.07).set_trans(Tween.TRANS_EXPO)
	TW.tween_property(self, "scale", Vector2.ZERO, 0.2).set_trans(Tween.TRANS_CUBIC)
	await TW.finished
	queue_free()
