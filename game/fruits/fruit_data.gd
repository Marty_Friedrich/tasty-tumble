extends Resource
class_name FruitData

@export var name:String
@export var nextFruit:PackedScene
@export var pointValue:int = 1
