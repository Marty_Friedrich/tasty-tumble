extends Fruit
class_name LegacyFruit


var color:Color

var radius:float

##Type of the fruit (Only used in legacy mode)
@export var legacyType:int:
	set(new):
		legacyType = new
		if !Engine.is_editor_hint():
			$Collision.shape = $Collision.shape.duplicate()
			$Collision.shape.radius = G.legacy.radius[new]
			radius = G.legacy.radius[new]
			color = G.legacy.colors[new]
			$Face.scale = Vector2(radius/230, radius/230)
			if get_groups().size() > 0:
				remove_from_group(get_groups()[0])
				add_to_group(str(legacyType))
			else:
				add_to_group(str(legacyType))


func _draw():
	draw_circle(Vector2.ZERO, radius, color)


func body_entered(body):
	if body.is_in_group(get_groups()[0]) and get_parent() == body.get_parent() and body.freeze == false and body.isPopping == false:
		isPopping = true
		body.isPopping = true
		var selfPos:Vector2 = global_position
		var otherPos:Vector2 = body.global_position
		var newPos:Vector2 = (selfPos + otherPos) / 2
		var otherRot:float = body.global_rotation
		var selfRot:float = global_rotation
		var selfVel:Vector2 = linear_velocity
		var otherVel:Vector2 = body.linear_velocity
		var parent:Node2D = get_parent()
		call_deferred("pop")
		body.call_deferred("pop")
		var duplicat:GPUParticles2D = get_parent().get_parent().get_parent().get_node("GameManager").boom.duplicate()
		duplicat.global_position = newPos
		get_parent().get_parent().get_node("Pop").play_random()
		get_parent().get_parent().get_node("Booms").add_child(duplicat)
		get_tree().current_scene.get_node("GameManager").add_score(G.legacy.points[legacyType])
		
		if legacyType + 1 < G.legacy.amount:
			var newFruit:LegacyFruit = load("res://fruits/legacy_fruit.tscn").instantiate().duplicate()
			newFruit.legacyType = legacyType + 1
			newFruit.global_position = newPos
			newFruit.global_rotation = lerp_angle(otherRot, selfRot, 0.5)
			newFruit.linear_velocity = lerp(selfVel, otherVel, 0.5)
			parent.call_deferred("add_child", newFruit)
