@tool
extends Node2D


@export var type:int

@export var legacy:LegacyData

@export var drawCircle:bool = true:
	set(new):
		_ready()

func _ready():
	var col:CollisionShape2D = CollisionShape2D.new()
	col.shape = CircleShape2D.new()
	col.shape.radius = %GameManager.legacy.radius[type]
	add_child(col)
	col.set_owner(get_tree().edited_scene_root)


func _process(delta):
	if get_child(0):
		get_child(0).shape.radius = legacy.radius[type]
