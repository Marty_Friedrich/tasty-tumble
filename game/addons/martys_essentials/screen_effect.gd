extends ColorRect


@export var effects:Array[Shader]



func line_up(duration:float = 0.4):
	var TW:Tween = create_tween()
	TW.tween_method(set_parameter.bind("transparency"), ["transparency", 1.0], ["transparency", 0.0], duration/2);
	TW.tween_method(set_parameter.bind("transparency"), ["transparency", 0.0], ["transparency", 1.0], duration/2);


func set_parameter(value:Variant, key:String):
	material.set_shader_parameter(key, value)
