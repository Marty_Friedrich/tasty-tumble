extends Node


func _ready():
	process_mode = Node.PROCESS_MODE_ALWAYS

var temp:Dictionary = {
	"FRUIT_MODE" = 0,
	"MODE" = 0,
	"RUNNING" = false,
}

var legacy:LegacyData = load("res://fruits/legacy_data.tres")


func modulate_to(ui:Control, color:Color = Color.WHITE, duration:float = 0.2):
	create_tween().tween_property(ui, "modulate", color, duration)

func fade_in(ui:CanvasItem, duration:float = 0.2):
	if ui.visible == true and ui.modulate == Color.WHITE:
		print(ui.name + " is already visibel, will not show")
		return
	ui.modulate = Color.TRANSPARENT
	ui.visible = true
	var TW:Tween = create_tween()
	TW.tween_property(ui, "modulate", Color.WHITE, duration)
	await TW.finished

func fade_out(ui:CanvasItem, duration:float = 0.2):
	if ui.visible == false:
		return
	var TW:Tween = get_tree().create_tween()
	TW.tween_property(ui, "modulate", Color.TRANSPARENT, duration)
	await TW.finished
	ui.visible = false

