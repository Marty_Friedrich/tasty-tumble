extends Control


@export var disableInGame:Array[MarginContainer]

@export var fruitSprites:Array[Texture2D]

func _ready():
	set_process(false)
	if Engine.is_editor_hint():
		$Bottom/Panel.set_anchors_preset(Control.PRESET_FULL_RECT)
		return
	$Bottom/Panel.set_anchors_preset(Control.PRESET_FULL_RECT)
	for i in $Bottom/Panel/ScrollContainer/List.get_children():
		var label:Label = i.get_node("Content/Label")
		if SaveSystem.has(label.text):
			var content:Control = i.get_node("Content").get_child(2)
			if content is OptionButton:
				content.selected = SaveSystem.get_var(label.text)
			if content is TextEdit:
				content.text = SaveSystem.get_var(label.text)
			if content is CheckButton or content is CheckBox:
				content.button_pressed = SaveSystem.get_var(label.text)
			if content is Slider:
				content.value = SaveSystem.get_var(label.text)
		if i.has_node("Content/Info"):
			if i.get_node("Content/Info").info:
				i.get_node("Content/Info").pressed.connect(display_info.bind(i.get_node("Content/Info").info))
	
	apply_settings()

func display_info(data:InfoData):
	%Info.get_node("Panel/Label").text = data.header
	%Info.get_node("Panel/RichTextLabel").text = data.content
	if timer == 0.0:
		%Info.get_node("AnimationPlayer").play("show")
		%Info.modulate = Color.TRANSPARENT
		var TW:Tween = create_tween()
		TW.tween_property(%Info, "modulate", Color.WHITE, 0.2)
	else:
		%Info.get_node("AnimationPlayer").play("update")
	set_process(true)
	timer = data.readTime


var timer:float = 0.0
func _process(delta):
	timer -= delta
	if timer <= 0:
		%Info.get_node("AnimationPlayer").play_backwards("show")
		var TW:Tween = create_tween()
		TW.tween_property(%Info, "modulate", Color.TRANSPARENT, 0.2)
		timer = 0.0
		set_process(false)


func save(continueGame:bool = true):
	if continueGame == true:
		for i in $Bottom/Panel/ScrollContainer/List.get_children():
			var label:Label = i.get_node("Content/Label")
			var content:Control = i.get_node("Content").get_child(2)
			if content is OptionButton:
				SaveSystem.set_var(label.text, content.selected)
			if content is TextEdit:
				SaveSystem.set_var(label.text, content.text)
			if content is CheckButton or content is CheckBox:
				SaveSystem.set_var(label.text, content.button_pressed)
			if content is Slider:
				SaveSystem.set_var(label.text, content.value)
		SaveSystem.save()
		apply_settings()
		%GameManager.unpause_game()
	get_node("AnimationPlayer").play("close")
	if timer > 0:
		$Info/AnimationPlayer.play_backwards("show")
	timer = 0.0
	

func apply_settings():
	if SaveSystem.has("LANGUAGE"):
		match SaveSystem.get_var("LANGUAGE"):
			0:
				TranslationServer.set_locale("en-US")
			1:
				TranslationServer.set_locale("de-DE")
	if SaveSystem.has("DIFFICULTY"):
		$"../../Node2D/Hand/FruitOrder".maxDrop = SaveSystem.get_var("DIFFICULTY")
	%Controller._ready()


func reset_all():
	SaveSystem.delete_all()
	SaveSystem.save()
	get_tree().reload_current_scene()

func close_reset_confirm():
	%ResetConfirm.visible = false
	%GameManager.unpause_game()






func request_reset():
	save(false)
	%ResetConfirm.visible = true


func difficulty_drag_start():
	G.fade_in(%SettingsVisual)


func difficulty_drag_end(_value_changed):
	G.fade_out(%SettingsVisual)


func difficulty_slider_changed(value):
	%SettingsVisual.get_node("Sprite").texture = fruitSprites[value]
	if value == 3:
		%SettingsVisual/ScoringInfo/Label.text = "RATED_SCORE"
	else:
		%SettingsVisual/ScoringInfo/Label.text = "UNRATED_SCORE"
