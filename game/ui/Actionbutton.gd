extends Button
class_name ActionButton

@export var action:String

func _ready():
	button_down.connect(press)
	button_up.connect(release)

func press():
	Input.action_press(action)

func release():
	Input.action_release(action)
