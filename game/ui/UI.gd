extends Control


func _ready():
	if SaveSystem.has("score"):
		SaveSystem.set_var("TASTY_SCORE", SaveSystem.get_var("score"))
		SaveSystem.delete("score")
	if SaveSystem.has("TASTY_SCORE"):
		%TastyScore.text = str(SaveSystem.get_var("TASTY_SCORE"))
	if SaveSystem.has("LEGACY_SCORE"):
		%LegacyScore.text = str(SaveSystem.get_var("LEGACY_SCORE"))

func open_settings():
	if %Settings.visible == true:
		%Settings.get_node("AnimationPlayer").play("close")
	%Settings.get_node("AnimationPlayer").play("open")

func start_game():
	$"../../GameManager".start_game()
	if SaveSystem.has("CONTROL_SCHEMA"):
		if SaveSystem.get_var("CONTROL_SCHEMA") == 1:
			%ButtonControls.visible = true
			%ButtonControls.get_node("Container").modulate = Color.TRANSPARENT
			var buttonTween:Tween = create_tween()
			buttonTween.tween_property(%ButtonControls.get_node("Container"), "modulate", Color.WHITE, 0.3)
	var TW:Tween = create_tween()
	TW.tween_property(self, "modulate", Color.TRANSPARENT, 0.3)
	await TW.finished
	visible = false
