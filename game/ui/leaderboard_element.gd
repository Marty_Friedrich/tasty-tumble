extends Control
class_name LeaderboardElement

@export var score:Score

func apply_score(_score:Score):
	%Points.text = str(_score.points)
	%Date.text = str(_score.date)
	#%Placement.text = str(_placement)

func get_score() -> int:
	return int(%Score.text)

func set_placement(_placement:int):
	%Placement.text = str(_placement)

func get_placement() -> int:
	return int(%Placement.text)
