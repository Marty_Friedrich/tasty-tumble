extends Button




@export var modes:Array[String]


func _ready():
	switch_mode(G.temp["FRUIT_MODE"])

func switch_mode(to:int = G.temp["FRUIT_MODE"] + 1):
	G.temp["FRUIT_MODE"] = to
	if G.temp["FRUIT_MODE"] >= modes.size():
		G.temp["FRUIT_MODE"] = 0

	text = modes[G.temp["FRUIT_MODE"]]
	
