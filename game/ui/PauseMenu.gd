extends Control

@export var scoreLabel:Label

func popup():
	visible = true
	$AnimationPlayer.play("popup")
	if !%GameManager.score == int(scoreLabel.text):
		$Center/VBoxContainer/SaveScore.text = "SAVE_SCORE"


func close():
	$AnimationPlayer.play_backwards("popup")
	await $AnimationPlayer.animation_finished
	visible = false



func open_settings():
	close()
	$"../Settings/AnimationPlayer".play("open")


func save_score():
	$Center/VBoxContainer/SaveScore.text = "SCORE_SAVED"
	%GameManager.save_score()
