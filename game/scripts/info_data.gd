extends Resource
class_name InfoData

@export var header:String

@export_multiline var content:String

@export var readTime:float = 3.0
