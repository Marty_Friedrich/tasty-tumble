extends Node

@export var legacy:LegacyData

@onready var boom:GPUParticles2D = preload("res://environment/boom.tscn").instantiate()

func start_game():

	G.fade_in(%Hand)
	if SaveSystem.has("LEGACY_MODE"):
		G.temp["MODE"] = SaveSystem.get_var("LEGACY_MODE")
	if SaveSystem.get_var("DIFFICULTY") != 3 and G.temp["MODE"] == 0:
		G.fade_in(%SaveDisclaimerMargin)
		get_tree().create_timer(10.0).timeout.connect(G.fade_out.bind(%SaveDisclaimerMargin))
	%InGame.visible = true
	%InGame.get_node("Control").modulate = Color.TRANSPARENT
	var TW:Tween = create_tween()
	TW.tween_property(%InGame.get_node("Control"), "modulate",  Color.WHITE, 0.2)
	$"../Node2D/Hand/Controller".canMove = true
	$"../Node2D/Hand/HandMovement".show_fruit()
	%ExtraPointsMargin.visible = false
	for i in %Settings.disableInGame:
		i.set_deferred("visible", false)



func unpause_game():
	G.temp["RUNNING"] = true
	get_tree().paused = false
	$"../Node2D/Hand/Controller".canMove = true
	var TW:Tween = create_tween()
	TW.tween_property(%GameUIMod, "color", Color.WHITE, 0.2)
	%PauseMenu.close()
	check_settings()

func check_settings():
	match %Controller.controlSchema:
		"Slide":
			%ButtonControls.visible = false
		"Buttons":
			%ButtonControls.visible = true

func _input(event):
	if event is InputEventKey and event.is_action("pause_game") and Input.is_action_just_pressed("pause_game"):
		match get_tree().paused:
			false:
				pause_game()
			true:
				unpause_game()

func pause_game():
	G.temp["RUNNING"] = false
	$"../Node2D/Hand/Controller".canMove = false
	%PauseMenu.popup()
	get_tree().paused = true
	var TW:Tween = create_tween()
	TW.tween_property(%GameUIMod, "color", Color.TRANSPARENT, 0.2)
	

@onready var movement:Node = $"../Node2D/Hand/HandMovement"

func retry():
	G.temp["RUNNING"] = false
	save_score()
	movement.reset()
	reset_score()
	G.fade_in(%UI)
	%InGame.visible = false
	%ExtraPointsMargin.visible = false
	G.fade_out(%LoseScreen)
	G.fade_out(%Hand)
	unpause_game()
	G.fade_out(%SaveDisclaimerMargin)
	for i in %Settings.disableInGame:
		i.set_deferred("visible", true)
	
	for i in %Fruits.get_children():
		i.call_deferred("queue_free")
		i.get_parent().call_deferred("remove_child", i)
	if SaveSystem.has("TASTY_SCORE"):
		%TastyScore.text = str(SaveSystem.get_var("TASTY_SCORE"))
	if SaveSystem.has("LEGACY_SCORE"):
		%LegacyScore.text = str(SaveSystem.get_var("LEGACY_SCORE"))
	#get_tree().reload_current_scene()

func lose():
	print("you lost.")
	G.fade_in(%LoseScreen)
	G.fade_out(%Hand)
	save_score()
	get_tree().paused = true


func save_score():
	if %Hand.get_node("FruitOrder").maxDrop != 3 and G.temp["MODE"] == 0:
		return
	var saveName:String = modes[G.temp["MODE"]]
	if SaveSystem.has(saveName):
		if SaveSystem.get_var(saveName) < score:
			SaveSystem.set_var(saveName, score)
	else:
		SaveSystem.set_var(saveName, score)
	SaveSystem.save()
	

var score:int = 0

const margins:Array[String] = ["margin_left", "margin_right", "margin_top", "margin_bottom"]

const modes:Array[String] = ["TASTY_SCORE", "LEGACY_SCORE"]

var isHighscore:bool = false

func reset_score():
	score = 0
	%Score.text = str(0)
	

func add_score(amount:int = 1):
	score += amount
	%Score.text = str(score)
	%Score.label_settings.font_size = 90
	get_tree().create_tween().tween_property(%Score, "label_settings:font_size", 30.0, 0.2)
	print("tries to be valid")
	if SaveSystem.has(modes[G.temp["MODE"]]) and score > SaveSystem.get_var(modes[G.temp["MODE"]]):
		print("is valid")
		if SaveSystem.has("DIFFICULTY") and SaveSystem.get_var("DIFFICULTY") != 3 and G.temp["MODE"] == 0:
			print("Skips cool effect")
			return
		%ExtraPointsMargin.visible = true
		for i in margins:
			%ExtraPointsMargin.set_deferred("theme_override_constants:" + i, 0.0)
			get_tree().create_tween().tween_method(tween_margin.bind(i), 5.0, 10.0, 0.2)
		if isHighscore == false:
			%ExtraPoints.text = "NEW_HIGHSCORE"
			%ExtraPointsPoof.emitting = true
			await get_tree().create_timer(2.0).timeout
		%ExtraPoints.text = "+" + str(score - SaveSystem.get_var(modes[G.temp["MODE"]]))
		isHighscore = true

func tween_margin(to:float, margin:String):
	%ExtraPointsMargin.add_theme_constant_override(margin, to)


func stop_game():
	$"../Node2D/Hand/Controller".canMove = false


func open_info():
	get_tree().change_scene_to_file("res://ui/info_area.tscn")






