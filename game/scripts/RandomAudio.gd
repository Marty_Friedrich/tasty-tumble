@tool
extends AudioStreamPlayer

class_name RandomAudio

@export var audios:Array[AudioStream]

@export var test:bool = true:
	set(new):
		if Engine.is_editor_hint():
			test = true
			play_random()

var lastSound:AudioStream

func play_random():
	randomize()
	var random: AudioStream
	while true:
		random = audios.pick_random()
		if random != lastSound:
			break
	stream = random
	lastSound = random
	play()
