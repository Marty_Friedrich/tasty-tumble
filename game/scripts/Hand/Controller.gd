extends Node


@export var slideSensetivity:float = 1.0
@export var buttonSensetivity:float = 1.0

var canMove:bool = false

@export_enum("Slide", "Buttons") var controlSchema:String = "Slide"

var isHolding:bool = false

func _ready():
	if SaveSystem.has("CONTROL_SCHEMA"):
		match SaveSystem.get_var("CONTROL_SCHEMA"):
			0:
				controlSchema = "Slide"
				%ButtonControls.visible = false
				%InGame/Control/Bottom.visible = true
				set_process(false)
			1:
				controlSchema = "Buttons"
				%InGame/Control/Bottom.visible = false
				set_process(true)
	if SaveSystem.has("BUTTON_SPEED"):
		buttonSensetivity = SaveSystem.get_var("BUTTON_SPEED")

func _process(delta):
	var movement:float = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	$"../HandMovement".move(movement * buttonSensetivity * delta * 100)
	$"../Line".material.set_shader_parameter("transparency", get_parent().modulate.a)

func _input(event):
	if event.is_action("shoot") and event.is_pressed():
		$"../HandMovement".drop_fruit()
	
	if event.is_action("shoot") and Input.is_action_just_pressed("shoot") == false:
		$"../HandMovement".drop_fruit()
	
	if event is InputEventScreenDrag:
		match controlSchema:
			"Slide":
				if event.position.y < $CanvasLayer/Control/SlideAbove.global_position.y:
					$"../HandMovement".move(event.relative.x * slideSensetivity)
