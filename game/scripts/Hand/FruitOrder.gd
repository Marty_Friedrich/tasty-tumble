extends Node

##There are the following fruits:
##Melon
##Pomelo
##Lemon
##Orange
##Tomato
@export var order:Array[String]

##The best item that the player can drop in the glass
@export_range(0, 6) var maxDrop:int

@onready var legacyFruit:LegacyFruit = preload("res://fruits/legacy_fruit.tscn").instantiate()

var fruits:Array[PackedScene]

func _ready():
	for i in order:
		var fruit:PackedScene = load("res://fruits/" + i + "/" + i.to_lower() + ".tscn")
		fruits.append(fruit)


func pick_random_fruit() -> Fruit:
	randomize()
	match G.temp["MODE"]:
		0:
			var random:int = randi_range(0, maxDrop)
			return fruits[random].instantiate().duplicate()
		1:
			var random:int = G.legacy.pick_fruit()
			var duplicat:LegacyFruit = legacyFruit.duplicate()
			duplicat.legacyType = random
			return duplicat
		_:
			var random = randi_range(0, maxDrop)
			return fruits[random].instantiate().duplicate()

