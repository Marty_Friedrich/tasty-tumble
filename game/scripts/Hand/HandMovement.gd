extends Node

##The speed at which the hand will move with button input
@export var speed:float = 1.0
@export_range(0, 1000) var vibDuration:int = 20

@onready var leftLimit:float = $LeftLimit.global_position.x
@onready var rightLimit:float = $RightLimit.global_position.x

var controlSchema:int = 0
# 0 = slide
# 1 = buttons
func set_control_type(type:int):
	controlSchema = type

func move(movement:float) -> void:
	get_parent().position.x += movement * speed
	get_parent().position.x = clamp(get_parent().position.x, leftLimit + fruitRadius, rightLimit - fruitRadius)


var currentFruit:RigidBody2D

var fruitRadius:float = 20.0

func show_fruit():
	match G.temp["MODE"]:
		0:
			if currentFruit:
				return
			var fruit:Fruit = $"../FruitOrder".pick_random_fruit()
			if fruit.get_node("Circle").shape is CircleShape2D or fruit.get_node("Circle").shape is CapsuleShape2D:
				print("is circle shape!")
				fruitRadius = fruit.get_node("Circle").shape.radius
			else: print("wetf")
			
			move(0.0)
			for i in fruit.get_children():
				if i is CollisionShape2D:
					i.disabled = true
			fruit.freeze = true
			get_parent().get_node("FruitPos").add_child(fruit)
			fruit.global_position = get_parent().get_node("FruitPos").global_position
			var TW:Tween = create_tween()
			fruit.scale = Vector2(0.0, 0.0)
			TW.tween_property(fruit, "scale", Vector2.ONE, 0.2).set_trans(Tween.TRANS_CUBIC)
			currentFruit = fruit
		1:
			if currentFruit:
				return
			var fruit:LegacyFruit = $"../FruitOrder".pick_random_fruit()
			fruitRadius = G.legacy.radius[fruit.legacyType]
			move(0.0)
			fruit.get_node("Collision").disabled = true
			fruit.freeze = true
			get_parent().get_node("FruitPos").add_child(fruit)
			fruit.global_position = get_parent().get_node("FruitPos").global_position
			var TW:Tween = create_tween()
			fruit.scale = Vector2(0.0, 0.0)
			TW.tween_property(fruit, "scale", Vector2.ONE, 0.2).set_trans(Tween.TRANS_CUBIC)
			currentFruit = fruit
	
	

func reset():
	currentFruit = null
	if $"../FruitPos".get_children().size() > 0:
		$"../FruitPos".remove_child($"../FruitPos".get_child(0))

func drop_fruit():
	if currentFruit:
		Input.vibrate_handheld(vibDuration)
		%Glass.play_random()
		for i in currentFruit.get_children():
			if i is CollisionShape2D:
				i.disabled = false
		currentFruit.reparent($"../../Fruits")
		currentFruit.freeze = false
		currentFruit = null
		await get_tree().create_timer(0.6).timeout
		show_fruit()
