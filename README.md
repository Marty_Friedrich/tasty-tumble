**Tasty Tumble**

A SUIKA type game made using the [Godot Game Engine](https://godotengine.org/)

This version of the SUIKA Game concept is a lot more simplified then you might know if from [some other games](https://suikagame.com/). This was done intentionally so that it is more fun as a quick mobile game.

I used the [latest and greatest Godot version 4.2](https://github.com/godotengine/godot/releases/tag/4.2-stable) for the project and all the art is made by me using [Krita](https://krita.org/en/) and all sounds were also recorded by me using [Audacity](https://www.audacityteam.org/). I'm using the [SaveSystem by AdamKormos](https://github.com/AdamKormos/SaveMadeEasy) published under the MIT lisence.

The project is currently still in its early shoes and big changes are still easy to do, so feel free to contribute, look at the code and do all the other GPL stuff you wanna do.
